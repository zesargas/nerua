# nerua

## Nota previa

Esta
proyecto
de
prueba
no
ha
sido
dockerizado.
Normalmente,
utilizo
Laradock
para
dockerizar
proyectos
en
Symfony
y
en
Laravel.
Pero
esta
prueba
Se
ha
creado
dentro
de
una
de
tantas
aplicaciones/servidores
que
existen
para
la
creación
de
proyectos
en
PHP:
XAMPP,
LAMP,
MAMP...
En
mi
caso,
he
utilizado
MAMP
desde
un
entorno
Apple.

Se
da
por
hecho
que
para
la
instalación
de
este
proyecto,
se
tienen
instalados
globalmente
en
el
equipo
tanto
Composer
como
Git

## Instalación y ejecución del proyecto

- Nos
  situamos
  por
  conola
  en
  la
  carpeta
  donde
  se
  alojan
  los
  proyectos
  en
  MAMP
  :

```
cd /Applications/MAMP/htdocs
```

- Clonanos
  el
  proyecto
  desde
  gitlab:

```
git clone git@gitlab.com:zesargas/nerua.git
```

- Entramos
  en
  la
  carpeta
  del
  proyecto
  proyecto

```
cd nerua
```

- A
  partir
  de
  ahora,
  todos
  los
  comando
  que
  ejecutemos
  desde
  la
  consola,
  lo
  haremos
  desde
  la
  carpeta
  raiz
  del
  proyecto:

```
/Applications/MAMP/htdocs/nerua
```

- Referenciamos
  el
  repositorio
  local
  con
  el
  remoto

```
git fetch
```

- Nos
  descargamos
  la
  rama
  master
  del
  repositorio
  remoto

```
git checkout -b master origin/master
```

- Actualizamos
  las
  depencencias

```
composer install
```

Creamos
la
base
de
datos

```
php bin/console doctrine:database:create
```

Para
no
tener
que
lanzar
las
migraciones
y
luego
crear
y
ejecutar
Fixtures,
lo
más
sencillo
es
entrar
en
la
base
de
datos
auren_db
recien
creada,
bien
a
través
del
servidor
MySQL
o
desde
la
aplicacion
phpMyAdmin
que
viene
en
el
MAMP
e
importar
el
archivo
auren_db.sql
que
viene
dentro
de
la
carpeta
raiz
de
nuestro
proyecto.
De
este
modo,
ya
se
nos
crearán
todos
los
tablas,
registros
y
usuarios
para
cacharrear
por
el
proyecto.

## Levantamos el proyecto

```
symfony server:start
```

## Para cacharrear por el proyecto...

Disponemos
de
dos
usuarios:

```
Email:      admin@gmail.com
Password:   adminauren
Rol:        ROLE_ADMIN
```

```
Email:      user@gmail.com
Password:   userauren
Rol:        ROLE_USER
```

Al
entrar
en
el
apartado
admin
(
solo
usuarios
con
el
rol
ROLE_ADMIN
tienen
acceso
)
previamente
se
realiza
una
consulta
a
la
API
https://restcountries.com/
para
actualizar
nuestra
base
de
datos
en
caso
de
que
en
la
API
haya
algún
pais
nuevo
que
no
esté
incorporado
en
nuestra
base
de
datos.
Para
comprobar
que
dicha
consulta
funciona
y
que
los
datos
se
actualizan
con
la
API
a
la
que
consultamos,
se
puede
vaciar
la
tabla
Pais
y
volver
a
entrar
al
apartado
administracion

---
